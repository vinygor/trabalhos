﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo3.App
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        void AbrirTela(UserControl tela)
        {
            if (panelConteudo.Controls.Count > 0)
                panelConteudo.Controls.RemoveAt(0);

            panelConteudo.Controls.Add(tela);
        }

        private void cadastrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Telas.frmProdutoCadastrar tela = new Telas.frmProdutoCadastrar();
            AbrirTela(tela);
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Telas.frmProdutoConsultar tela = new Telas.frmProdutoConsultar();
            AbrirTela(tela);
        }

        private void cadastrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Telas.frmPedidoCadastrar tela = new Telas.frmPedidoCadastrar();
            AbrirTela(tela);
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Telas.frmPedidoConsultar tela = new Telas.frmPedidoConsultar();
            AbrirTela(tela);
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Produto;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmProdutoConsultar : UserControl
    {
        public frmProdutoConsultar()
        {
            InitializeComponent();
        }

        

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Consultar(txtProduto.Text);

            dgvProdutos.AutoGenerateColumns = false;
            dgvProdutos.DataSource = lista;
        }
    }

}




